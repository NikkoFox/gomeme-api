package image

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/nikkofox/gomeme-api/image/stream"
	"gitlab.com/nikkofox/gomeme-api/models"
)

// Load - image type detection and load to stream.Stream
func Load(meme models.Meme) (stream.Stream, error) {
	var s io.Reader
	switch {
	case isURL(meme.Image):
		s, _ = downloadURL(meme.Image)
	case isLocalFile(meme.Image):
		s, _ = readFile(meme.Image)
	case isFormFile(meme.Image, meme.ImageForm):
		s, _ = readFormFile(meme.ImageForm)
	case isDataURIBase64(meme.Image):
		s, _ = readDataURIBase64(meme.Image)
	default:
		return stream.Stream{}, errors.New("image not recognized")
	}
	return stream.NewStream(s)
}

// Return true if the passed string is an image URL, false if not.
func isURL(url string) bool {
	return strings.HasPrefix(url, "http")
}

// Download the image located at the passed image URL, decode and return it.
func downloadURL(url string) (io.Reader, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = res.Body.Close()
	}()

	if res.StatusCode != 200 {
		return nil, errors.New("could not access URL")
	}

	st, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.New("could not read response body")
	}

	return bytes.NewReader(st), err
}

// Return true if the passed string is a file that exists on the local
// filesystem, false if not.
func isLocalFile(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

// Read and return a file on the local filesystem.
// The file is assumed to exist.
func readFile(path string) (io.Reader, error) {
	st, err := ioutil.ReadFile(filepath.Clean(path))
	return bytes.NewReader(st), err
}

// Return true if image string is empty and imageForm is not nil, false if not.
func isFormFile(image string, imageForm multipart.File) bool {
	return image == "" && imageForm != nil
}

// Read and return a file from multipart.File.
func readFormFile(image multipart.File) (io.Reader, error) {
	st, err := ioutil.ReadAll(image)

	if err == nil {
		return bytes.NewReader(st), err
	}
	return nil, err
}

// Return true if image string is dataURI and base64 encoding
func isDataURIBase64(image string) bool {
	return strings.HasPrefix(image, "data:image/") && strings.Contains(image, "base64,")
}

// Read and return a file from dataURI base64 image
func readDataURIBase64(image string) (io.Reader, error) {
	b64data := image[strings.IndexByte(image, ',')+1:]
	st, err := base64.StdEncoding.DecodeString(b64data)
	if err == nil {
		return bytes.NewReader(st), err
	}
	return nil, err
}
