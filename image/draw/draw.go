// Package draw contains functionality for drawing text
package draw

import (
	"image"
	"math"
	"strings"

	"gitlab.com/nikkofox/gomeme-api/font"

	"github.com/fogleman/gg"
)

const (
	fontBorderRadius  = 3.0   // px
	fontLeading       = 1.42  // percentage
	maxFontSize       = 100.0 // pts
	topTextDivisor    = 5.0   // divisor
	bottomTextDivisor = 3.75  // divisor
	imageMargin       = 23.0  // px
)

// NewContext creates a new context for the passed image
func NewContext(img image.Image) *gg.Context {
	return gg.NewContextForImage(img)
}

// TopBanner draws the top text onto the meme.
func TopBanner(ctx *gg.Context, text string, fastStroke bool) {
	x := float64(ctx.Width()) / 2
	y := imageMargin
	drawText(ctx, text, x, y, 0.5, 0.0, topTextDivisor, fastStroke)
}

// BottomBanner draws the bottom text onto the meme.
func BottomBanner(ctx *gg.Context, text string, fastStroke bool) {
	x := float64(ctx.Width()) / 2
	y := float64(ctx.Height()) - imageMargin
	drawText(ctx, text, x, y, 0.5, 1.0, bottomTextDivisor, fastStroke)
}

// Draw text onto the meme.
func drawText(ctx *gg.Context, text string, x, y, ax, ay, divisor float64, fastStroke bool) {
	text = strings.ToUpper(text)
	width := float64(ctx.Width()) - (imageMargin * 2)
	height := float64(ctx.Height()) / divisor
	calculateFontSize(ctx, text, width, height)

	// Draw the text border.
	if fastStroke {
		drawFastStroke(ctx, text, x, y, ax, ay, width)
	} else {
		drawAccurateStroke(ctx, text, x, y, ax, ay, width)
	}

	// Draw the text itself.
	ctx.SetHexColor("#FFF")
	ctx.DrawStringWrapped(text, x, y, ax, ay, width, fontLeading, gg.AlignCenter)
}

// Inaccurate but fast stroke
func drawFastStroke(ctx *gg.Context, text string, x, y, ax, ay, width float64) {
	ctx.SetHexColor("#000")
	ctx.DrawStringWrapped(text, x-2, y-2, ax, ay, width, fontLeading, gg.AlignCenter)
	ctx.DrawStringWrapped(text, x+2, y-2, ax, ay, width, fontLeading, gg.AlignCenter)
	ctx.DrawStringWrapped(text, x+2, y+2, ax, ay, width, fontLeading, gg.AlignCenter)
	ctx.DrawStringWrapped(text, x-2, y+2, ax, ay, width, fontLeading, gg.AlignCenter)
}

// Accurate but slow stroke
func drawAccurateStroke(ctx *gg.Context, text string, x, y, ax, ay, width float64) {
	ctx.SetHexColor("#000")
	for angle := 0.0; angle < (2 * math.Pi); angle += 0.35 {
		bx := x + (math.Sin(angle) * fontBorderRadius)
		by := y + (math.Cos(angle) * fontBorderRadius)
		ctx.DrawStringWrapped(text, bx, by, ax, ay, width, fontLeading, gg.AlignCenter)
	}
}

// Dynamically calculate the correct size needed for text.
func calculateFontSize(ctx *gg.Context, text string, width, height float64) {
	for size := maxFontSize; size > 20; size-- {
		var rWidth, rHeight float64
		var lWidth, lHeight float64

		_ = ctx.LoadFontFace(font.Path, size)

		lines := ctx.WordWrap(text, width)

		for _, line := range lines {
			lWidth, lHeight = ctx.MeasureString(line)
			if lWidth > rWidth {
				rWidth = lWidth
			}
		}

		rHeight = (lHeight * fontLeading) * float64(len(lines))
		if rWidth <= width && rHeight <= height {
			break
		}
	}
}
