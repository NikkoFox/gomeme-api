# GoMeme-API

Service puts your text on an image. Create [image macro](https://en.wikipedia.org/wiki/Image_macro) style memes.    
Based on [nomad-software/meme](https://github.com/nomad-software/meme).   

[![pipeline status](https://gitlab.com/nikkofox/gomeme-api/badges/develop/pipeline.svg)](https://gitlab.com/nikkofox/gomeme-api/-/commits/develop)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/nikkofox/gomeme-api)](https://goreportcard.com/report/gitlab.com/nikkofox/gomeme-api)
[![Hits-of-Code](https://hitsofcode.com/gitlab/nikkofox/gomeme-api)](https://hitsofcode.com/gitlab/nikkofox/gomeme-api/view)


## Usage example
![](https://i.imgur.com/hBK28bf.gif)

## Install
* Install Go
* Run go get -u -v gitlab.com/nikkofox/gomeme-api

## Docker
Docker image is available in the project [Container Registry](https://gitlab.com/nikkofox/gomeme-api/container_registry)
```bash
docker login registry.gitlab.com
docker run -p "7744:7744" -d registry.gitlab.com/nikkofox/gomeme-api/develop
```

--------

## Indices

* [POST image by FORM](#post-image-by-form)
* [POST image by JSON](#post-image-by-json)
* [Response](#response)

--------

### POST image by FORM


***Endpoint:***

```bash
Method: POST
Type: FORMDATA
URL: /meme/create
```

***Body:***

| Key | Type | Description |
|---|------|-------------|
| image | file form-data (Required) |  |
| top_text | string (Optional) |  |
| bottom_text | string (Optional) |  |
| fast_stroke | string (Optional, default: false) | Convert to bool. Inaccurate but fast stroke |



### POST image by JSON


***Endpoint:***

```bash
Method: POST
Type: JSON
URL: /meme/create
```



***Body:***

| Key | Type | Description |
| --- | ------|-------------|
| image | string (Required) | URL \| image path on server \| [Data URI](https://en.wikipedia.org/wiki/Data_URI_scheme) Base64 Encoded Image |
| top_text | string (Optional) |  |
| bottom_text | string (Optional) |  |
| fast_stroke | bool (Optional, default: false) | Inaccurate but fast stroke |


```js        
{
    "image": "https://via.placeholder.com/600", 
    "top_text": "sample top text",
    "bottom_text": "sample bottom text"
}
```

### Response

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | image/jpeg | Binary data stream for the responsive image. |

***

## Build with custom fonts

It is possible to add any font you want by copying the font into the fonts folder and passing FONT environment variable. In order to add your custom fonts follow the steps:

```bash
cp <your_font_path> data/fonts
docker build -t gomeme:latest .
docker run -p "7744:7744" -e FONT="<your_font_name>" gomeme:latest
```
