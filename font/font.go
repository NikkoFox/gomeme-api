// Package font processes the font file
package font

import (
	"os"
	"path/filepath"

	"gitlab.com/nikkofox/gomeme-api/data"
)

var (
	// Path is the location of the font file.
	Path string
)

// Write the embedded font to the temporary directory.
func init() {
	Path = filepath.Join(os.TempDir(), filepath.Base(data.Font))

	_, err := os.Stat(Path)
	if err != nil {
		file, err := os.Create(Path)
		if err != nil {
			panic(err)
		}
		defer func() {
			if err = file.Close(); err != nil {
				panic(err)
			}
		}()

		stream, err := data.Files.ReadFile(data.Font)
		if err != nil {
			panic(err)
		}

		_, err = file.Write(stream)
		if err != nil {
			panic(err)
		}
	}
}
